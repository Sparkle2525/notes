from django.contrib import messages
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render

from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser

from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout

# Create your views here.
from django.urls import reverse

from main_app.api import NoteSerializer, CategorySerializer
from main_app.models import Note, Category


def login(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')

        user = authenticate(username=email, password=password)
        if user is not None:
            auth_login(request, user)
            return HttpResponseRedirect(reverse('index'))
        else:
            if User.objects.filter(username=email).exists():
                messages.error(request, 'Неверный пароль')
            else:
                messages.error(request, 'Пользователь не существует')
            return HttpResponseRedirect(reverse('login'))
    return render(request, 'user/login.html', {})


def logout(request):
    auth_logout(request)
    return HttpResponseRedirect(reverse('login'))


def register(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')
        repeat_password = request.POST.get('repeat_password')

        if not all([email, password, repeat_password]):
            messages.error(request, 'Заполните все поля')
            return HttpResponseRedirect(reverse('register'))

        if User.objects.filter(username=email).exists():
            messages.error(request, 'Пользователь с данным email существует')
            return HttpResponseRedirect(reverse('register'))
        if password != repeat_password:
            messages.error(request, 'Пароли не совпадают')
            return HttpResponseRedirect(reverse('register'))

        User.objects.create_user(username=email, email=email, password=password)
        return render(request, 'user/reg_success.html', {})
    return render(request, 'user/register.html', {})


@login_required(login_url='/login')
def index(request):
    return render(request, 'index.html')


@csrf_exempt
def categories_list(request):
    categories = Category.objects.all()
    serializer = CategorySerializer(categories, many=True)
    return JsonResponse(serializer.data, safe=False)


@csrf_exempt
def notes_list(request):
    if request.method == 'GET':
        notes = Note.objects.filter(user=request.user)
        serializer = NoteSerializer(notes.order_by('-creation'), many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        data['user'] = {'id': request.user.id}
        serializer = NoteSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


@csrf_exempt
def note_detail(request, pk):
    try:
        note = Note.objects.get(pk=pk)
    except Note.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = NoteSerializer(note)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)

        serializer = NoteSerializer(note, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        note.delete()
        return HttpResponse(status=204)


@csrf_exempt
def add_to_favourites(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        try:
            note = Note.objects.get(id=data['id'])
        except Note.DoesNotExist:
            return HttpResponse(status=404)
        note.favourite = not note.favourite
        note.save()
        return HttpResponse(status=201)
    else:
        return HttpResponse(status=405)


@csrf_exempt
def publish(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        try:
            note = Note.objects.get(id=data['id'])
        except Note.DoesNotExist:
            return HttpResponse(status=404)
        note.published = not note.published
        note.save()
        return HttpResponse(status=201)
    else:
        return HttpResponse(status=405)


@csrf_exempt
def note_static_page(request, pk):
    try:
        note = Note.objects.get(pk=pk)
    except Note.DoesNotExist:
        return HttpResponse(status=404)
    if not note.published:
        return HttpResponse(status=404)
    return render(request, 'note_static.html', {'note': note})

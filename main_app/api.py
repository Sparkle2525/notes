from django.contrib.auth.models import User
from rest_framework import serializers, viewsets

from main_app.models import Category, Note


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = Category
        fields = ('id', 'name',)


class UserSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = User
        fields = ('id',)


class NoteSerializer(serializers.HyperlinkedModelSerializer):
    category = CategorySerializer()
    user = UserSerializer()

    class Meta:
        model = Note
        fields = ('id', 'user', 'title', 'content', 'creation', 'category', 'favourite', 'published')

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.content = validated_data.get('content', instance.content)
        instance.creation = validated_data.get('creation', instance.creation)
        instance.favourite = validated_data.get('favourite', instance.favourite)
        instance.published = validated_data.get('published', instance.published)

        category = validated_data.pop('category')
        if category:
            instance.category = Category.objects.get(id=category['id'])

        instance.save()
        return instance

    def create(self, validated_data):
        category = validated_data.pop('category')
        user = validated_data.pop('user')
        user = User.objects.get(id=user['id'])
        category = Category.objects.get(id=int(category['id']))
        note = Note.objects.create(category=category, user=user, **validated_data)

        return note


class NoteViewSet(viewsets.ModelViewSet):
    queryset = Note.objects.all()
    serializer_class = NoteSerializer


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

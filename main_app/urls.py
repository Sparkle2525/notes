from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from main_app.views import login, register, index, categories_list, add_to_favourites, publish, note_static_page, logout
from notes import settings
from rest_framework import routers

from main_app.api import NoteViewSet
from main_app.views import notes_list, note_detail


urlpatterns = [
    url(r'^login/', login, name='login'),
    url(r'^logout/', logout, name='logout'),
    url(r'^register/', register, name='register'),
    url(r'^$', index, name='index'),
    url(r'^api/notes/(?P<pk>[0-9]+)$', note_detail),
    url(r'^api/categories', categories_list),
    url(r'^api/notes', notes_list),
    url(r'^api/add_to_favourites', add_to_favourites),
    url(r'^api/publish', publish),
    url(r'^note/(?P<pk>[0-9]+)$', note_static_page)
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

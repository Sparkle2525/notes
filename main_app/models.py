from django.contrib.auth.models import User
from django.db import models
from redactor.fields import RedactorField


# Create your models here.


class Category(models.Model):
    name = models.CharField(max_length=250, verbose_name='Название')

    class Meta:
        verbose_name_plural = 'Категории'
        verbose_name = 'Категория'

    def __str__(self):
        return self.name


class Note(models.Model):
    user = models.ForeignKey(User, verbose_name='Пользователь')
    title = models.TextField(verbose_name='Заголовок')
    content = RedactorField(verbose_name='Текст заметки', allow_file_upload=True,
                            allow_image_upload=True)
    creation = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    category = models.ForeignKey(Category, verbose_name='Категория')
    favourite = models.BooleanField(default=False, verbose_name='Избранное')
    published = models.BooleanField(default=False, verbose_name='Опубликовано')

    class Meta:
        verbose_name_plural = 'Заметки'
        verbose_name = 'Заметка'

    def __str__(self):
        return self.title

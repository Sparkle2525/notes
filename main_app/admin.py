from django.contrib import admin

from django import forms
from redactor.widgets import RedactorEditor


# Register your models here.
from main_app.models import Note, Category


class NoteAdmin(admin.ModelAdmin):
    model = Note
    list_filter = ('title', 'creation', 'category', 'favourite')


class CategoryAdmin(admin.ModelAdmin):
    model = Category

admin.site.register(Note, NoteAdmin)
admin.site.register(Category, CategoryAdmin)

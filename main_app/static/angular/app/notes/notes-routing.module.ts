import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NotesComponent} from "./notes.component";
import {CreateComponent} from "./create/create.component";
import {UpdateComponent} from "./update/update.component";

const routes: Routes = [
    {path: '', redirectTo: 'notes', pathMatch: 'full'},
    {path: 'notes', component: NotesComponent},
    {path: 'notes/:id', component: UpdateComponent},
    {path: 'create', component: CreateComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class NotesRoutingModule {
}

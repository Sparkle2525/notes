"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
// Imports for loading & configuring the in-memory web api
var http_1 = require("@angular/http");
var notes_component_1 = require("./notes.component.js");
var notes_service_1 = require("./shared/notes.service.js");
var notes_routing_module_1 = require("./notes-routing.module.js");
var router_1 = require("@angular/router");
var platform_browser_1 = require("@angular/platform-browser");
var create_component_1 = require("./create/create.component.js");
var angular_froala_wysiwyg_1 = require("angular-froala-wysiwyg");
var update_component_1 = require("./update/update.component.js");
var angular2_moment_1 = require("angular2-moment");
var filter_service_1 = require("./shared/filter.service.js");
var NotesModule = (function () {
    function NotesModule() {
    }
    return NotesModule;
}());
NotesModule = __decorate([
    core_1.NgModule({
        imports: [
            angular_froala_wysiwyg_1.FroalaEditorModule.forRoot(), angular_froala_wysiwyg_1.FroalaViewModule.forRoot(),
            platform_browser_1.BrowserModule,
            angular2_moment_1.MomentModule,
            forms_1.FormsModule,
            http_1.HttpModule,
            router_1.RouterModule,
            notes_routing_module_1.NotesRoutingModule
        ],
        declarations: [
            notes_component_1.NotesComponent,
            create_component_1.CreateComponent,
            update_component_1.UpdateComponent
        ],
        providers: [
            notes_service_1.NoteService, filter_service_1.FilterService
        ]
    })
], NotesModule);
exports.NotesModule = NotesModule;
//# sourceMappingURL=notes.module.js.map
import {Component, OnInit} from '@angular/core';
import {NoteService} from "./shared/notes.service";
import {Category, Note} from "./shared/notes";
import {FilterService} from "./shared/filter.service";

@Component({
    selector: 'app-notes',
    templateUrl: './notes.component.html',
})
export class NotesComponent implements OnInit {
    notes: Note[];
    notesCopy: Note[];
    categories: Category[];
    linkUrl = 'http://localhost:8000/note/';
    constructor(private noteService: NoteService) {}
    ngOnInit(): void {
        this.noteService.getNotes().then(notes => {
            this.notes = notes;
            this.noteService.getCategories().then(cats => {
                this.categories = cats;
            });
        });
    }
    deleteNote(id: number): boolean {
        this.noteService.deleteNote(id).then(() => {
            this.notes = this.notes.filter(item => item.id !== id);
        });
        return false;
    }
    addToFavourites(note: Note): boolean {
        this.noteService.addToFavourites(note.id).then(() => {
            note.favourite = !note.favourite;
        });
        return false;
    }
    publish(note: Note): boolean {
        this.noteService.publish(note.id).then(() => {
            note.published = !note.published;
            if (!note.published && note.linkVisible) {
                note.linkVisible = false;
            }
        });
        return false;
    }
    filterDate(type: string): void {
        this.makeNotesCopy();
        this.notes = FilterService.date(this.notesCopy.slice(), type);
    }
    filterCategory(category: Category): void {
        this.makeNotesCopy();
        this.notes = FilterService.category(this.notesCopy.slice(), category);
    }
    filterFavourites(val: boolean): void {
        this.makeNotesCopy();
        this.notes = FilterService.favourite(this.notesCopy.slice(), val);
    }
    filterTitle(substring: string): void {
        if (substring === '') {
            return;
        }
        this.makeNotesCopy();
        this.notes = FilterService.title(this.notesCopy.slice(), substring);
    }
    sortDate(): void {
        this.notes.sort((a, b) => {
            if (a.creation < b.creation) {
                return 1;
            }
            if (a.creation > b.creation) {
                return -1;
            }
            return 0;
        });
    }
    sortCategory(): void {
        this.notes.sort((a, b) => {
            if (a.category.name > b.category.name) {
                return 1;
            }
            if (a.category.name < b.category.name) {
                return -1;
            }
            return 0;
        });
    }
    sortFavourites(): void {
        this.notes.sort((a, b) => {
            if (a.favourite < b.favourite) {
                return 1;
            }
            if (a.favourite > b.favourite) {
                return -1;
            }
            return 0;
        });
    }
    showLink(note: Note): boolean {
        note.linkVisible = !note.linkVisible;
        return false;
    }
    returnNotes(): void {
        this.makeNotesCopy();
        this.notes = this.notesCopy.slice();
    }
    makeNotesCopy(): void {
        if (!this.notesCopy) {
            this.notesCopy = this.notes.slice();
        }
    }
}

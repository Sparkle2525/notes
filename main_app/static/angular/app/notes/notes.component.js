"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var notes_service_1 = require("./shared/notes.service.js");
var filter_service_1 = require("./shared/filter.service.js");
var NotesComponent = (function () {
    function NotesComponent(noteService) {
        this.noteService = noteService;
        this.linkUrl = 'http://localhost:8100/note/';
    }
    NotesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.noteService.getNotes().then(function (notes) {
            _this.notes = notes;
            _this.noteService.getCategories().then(function (cats) {
                _this.categories = cats;
            });
        });
    };
    NotesComponent.prototype.deleteNote = function (id) {
        var _this = this;
        this.noteService.deleteNote(id).then(function () {
            _this.notes = _this.notes.filter(function (item) { return item.id !== id; });
        });
        return false;
    };
    NotesComponent.prototype.addToFavourites = function (note) {
        this.noteService.addToFavourites(note.id).then(function () {
            note.favourite = !note.favourite;
        });
        return false;
    };
    NotesComponent.prototype.publish = function (note) {
        this.noteService.publish(note.id).then(function () {
            note.published = !note.published;
            if (!note.published && note.linkVisible) {
                note.linkVisible = false;
            }
        });
        return false;
    };
    NotesComponent.prototype.filterDate = function (type) {
        this.makeNotesCopy();
        this.notes = filter_service_1.FilterService.date(this.notesCopy.slice(), type);
    };
    NotesComponent.prototype.filterCategory = function (category) {
        this.makeNotesCopy();
        this.notes = filter_service_1.FilterService.category(this.notesCopy.slice(), category);
    };
    NotesComponent.prototype.filterFavourites = function (val) {
        this.makeNotesCopy();
        this.notes = filter_service_1.FilterService.favourite(this.notesCopy.slice(), val);
    };
    NotesComponent.prototype.filterTitle = function (substring) {
        if (substring === '') {
            return;
        }
        this.makeNotesCopy();
        this.notes = filter_service_1.FilterService.title(this.notesCopy.slice(), substring);
    };
    NotesComponent.prototype.sortDate = function () {
        this.notes.sort(function (a, b) {
            if (a.creation < b.creation) {
                return 1;
            }
            if (a.creation > b.creation) {
                return -1;
            }
            return 0;
        });
    };
    NotesComponent.prototype.sortCategory = function () {
        this.notes.sort(function (a, b) {
            if (a.category.name > b.category.name) {
                return 1;
            }
            if (a.category.name < b.category.name) {
                return -1;
            }
            return 0;
        });
    };
    NotesComponent.prototype.sortFavourites = function () {
        this.notes.sort(function (a, b) {
            if (a.favourite < b.favourite) {
                return 1;
            }
            if (a.favourite > b.favourite) {
                return -1;
            }
            return 0;
        });
    };
    NotesComponent.prototype.showLink = function (note) {
        note.linkVisible = !note.linkVisible;
        return false;
    };
    NotesComponent.prototype.returnNotes = function () {
        this.makeNotesCopy();
        this.notes = this.notesCopy.slice();
    };
    NotesComponent.prototype.makeNotesCopy = function () {
        if (!this.notesCopy) {
            this.notesCopy = this.notes.slice();
        }
    };
    return NotesComponent;
}());
NotesComponent = __decorate([
    core_1.Component({
        selector: 'app-notes',
        templateUrl: '/static/angular/app/notes/notes.component.html',
    }),
    __metadata("design:paramtypes", [notes_service_1.NoteService])
], NotesComponent);
exports.NotesComponent = NotesComponent;
//# sourceMappingURL=notes.component.js.map
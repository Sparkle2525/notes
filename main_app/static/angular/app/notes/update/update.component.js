"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var notes_service_1 = require("../shared/notes.service.js");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
require("rxjs/add/operator/switchMap");
var UpdateComponent = (function () {
    function UpdateComponent(noteService, route, location) {
        this.noteService = noteService;
        this.route = route;
        this.location = location;
        this.options = {
            placeholderText: 'Edit Your Content Here!',
            charCounterCount: false,
            height: 300
        };
    }
    UpdateComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.subscription = this.route.paramMap
            .switchMap(function (params) { return _this.noteService.getNote(+params.get('id')); }).subscribe(function (note) {
            _this.noteService.getCategories().then(function (cats) {
                _this.note = note;
                _this.categories = cats;
                if (cats.length) {
                    for (var _i = 0, cats_1 = cats; _i < cats_1.length; _i++) {
                        var cat = cats_1[_i];
                        if (cat.id === note.category.id) {
                            _this.selectedCatObj = cat;
                        }
                    }
                }
            });
        });
    };
    UpdateComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    UpdateComponent.prototype.goBack = function () {
        this.location.back();
    };
    UpdateComponent.prototype.update = function () {
        var _this = this;
        this.note.category = this.selectedCatObj;
        this.noteService.updateNote(this.note).then(function () {
            _this.goBack();
        });
    };
    return UpdateComponent;
}());
UpdateComponent = __decorate([
    core_1.Component({
        selector: 'app-update',
        templateUrl: '/static/angular/app/notes/update/update.component.html'
    }),
    __metadata("design:paramtypes", [notes_service_1.NoteService, router_1.ActivatedRoute,
        common_1.Location])
], UpdateComponent);
exports.UpdateComponent = UpdateComponent;
//# sourceMappingURL=update.component.js.map
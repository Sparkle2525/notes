import {Component, OnInit} from '@angular/core';
import {Category, Note} from "../shared/notes";
import {NoteService} from "../shared/notes.service";
import {Location} from '@angular/common';

@Component({
    selector: 'app-create',
    templateUrl: './create.component.html',
})
export class CreateComponent implements OnInit {
    public options: Object = {
        placeholderText: 'Edit Your Content Here!',
        charCounterCount: false,
        height: 300
    };
    note: Note;
    categories: Category[];
    selectedCatObj: Category;
    constructor(private noteService: NoteService, private location: Location) {}
    ngOnInit(): void {
        this.note = new Note();
        this.noteService.getCategories().then(cats => {
            this.categories = cats;
            if (cats.length) {
                this.selectedCatObj = cats[0];
            }
        });
    }
    create(): void {
        if (!this.note.content || !this.note.title) {
            return;
        }
        this.note.category = this.selectedCatObj;
        this.noteService.createNote(this.note).then(() => {
            this.goBack();
        });
    }
    goBack(): void {
        this.location.back();
    }
}

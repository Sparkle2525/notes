"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var notes_1 = require("../shared/notes.js");
var notes_service_1 = require("../shared/notes.service.js");
var common_1 = require("@angular/common");
var CreateComponent = (function () {
    function CreateComponent(noteService, location) {
        this.noteService = noteService;
        this.location = location;
        this.options = {
            placeholderText: 'Edit Your Content Here!',
            charCounterCount: false,
            height: 300
        };
    }
    CreateComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.note = new notes_1.Note();
        this.noteService.getCategories().then(function (cats) {
            _this.categories = cats;
            if (cats.length) {
                _this.selectedCatObj = cats[0];
            }
        });
    };
    CreateComponent.prototype.create = function () {
        var _this = this;
        if (!this.note.content.replace(' ', '') || !this.note.title.replace(' ', '')) {
            return;
        }
        this.note.category = this.selectedCatObj;
        this.noteService.createNote(this.note).then(function () {
            _this.goBack();
        });
    };
    CreateComponent.prototype.goBack = function () {
        this.location.back();
    };
    return CreateComponent;
}());
CreateComponent = __decorate([
    core_1.Component({
        selector: 'app-create',
        templateUrl: '/static/angular/app/notes/create/create.component.html',
    }),
    __metadata("design:paramtypes", [notes_service_1.NoteService, common_1.Location])
], CreateComponent);
exports.CreateComponent = CreateComponent;
//# sourceMappingURL=create.component.js.map
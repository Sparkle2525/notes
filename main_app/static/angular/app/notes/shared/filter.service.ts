import {Injectable} from '@angular/core';

import {Category, Note} from './notes';
import 'rxjs/add/operator/toPromise';

function getWeekNumber(d: Date): number {
    d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
    d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay() || 7));
    let yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
    let weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1) / 7);

    return weekNo;
}


@Injectable()
export class FilterService {
    static date(notes: Note[], type: string): Note[] {
        switch (type) {
            case 'today': {
                notes = notes.filter(note => new Date(note.creation).getDate() === (new Date()).getDate());
                break;
            }
            case 'week': {
                notes = notes.filter(note => getWeekOfMonth(new Date(note.creation)) === getWeekOfMonth(new Date()));
                break;
            }
            case 'month': {
                notes = notes.filter(note => new Date(note.creation).getMonth() === (new Date()).getMonth());
                break;
            }
            case 'year': {
                notes = notes.filter(note => new Date(note.creation).getFullYear() === (new Date()).getFullYear());
                break;
            }
        }
        return notes;
    }
    static category(notes: Note[], category: Category): Note[] {
        return notes.filter(note => note.category.id === category.id);
    }
    static favourite(notes: Note[], val: boolean): Note[] {
        return notes.filter(note => note.favourite === val);
    }
    static title(notes: Note[], substring: string): Note[] {
        return notes.filter(note => note.title.indexOf(substring) !== -1);
    }
}

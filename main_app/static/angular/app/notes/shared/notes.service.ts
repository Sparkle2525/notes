import {Injectable} from '@angular/core';

import {Category, Note} from './notes';
import {Http} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {Headers} from '@angular/http';


@Injectable()
export class NoteService {
    private notesUrl = 'http://localhost:8000/api/notes';
    private addToFavouritesUrl = 'http://localhost:8000/api/add_to_favourites';
    private publishUrl = 'http://localhost:8000/api/publish';
    private categoriesUrl = 'http://localhost:8000/api/categories';

    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http: Http) {
    }

    getNotes(): Promise<Note[]> {
        return this.http.get(this.notesUrl)
            .toPromise()
            .then(response => response.json() as Note[])
            .catch(this.handleError);
    }
    getCategories(): Promise<Category[]> {
        return this.http.get(this.categoriesUrl)
            .toPromise()
            .then(response => response.json() as Category[])
            .catch(this.handleError);
    }
    getNote(id: number): Promise<Note> {
        const url = `${this.notesUrl}/${id}`;
        return this.http.get(url)
            .toPromise()
            .then(response => response.json() as Note)
            .catch(this.handleError);
    }
    createNote(note: Note): Promise<any> {
        return this.http
            .post(this.notesUrl, JSON.stringify(note), {headers: this.headers})
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }

    updateNote(note: any): Promise<Note> {
        // note.date.setHours(note.date.getHours() - note.date.getTimezoneOffset() / 60);
        const url = `${this.notesUrl}/${note.id}`;
        return this.http
            .put(url, JSON.stringify(note), {headers: this.headers})
            .toPromise()
            .then(response => response.json() as Note)
            .catch(this.handleError);
    }

    deleteNote(id: number): Promise<void> {
        const url = `${this.notesUrl}/${id}`;
        return this.http.delete(url, {headers: this.headers})
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }
    addToFavourites(id: number): Promise<void> {
        return this.http.post(this.addToFavouritesUrl, JSON.stringify({id: id}))
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }
    publish(id: number): Promise<void> {
        return this.http.post(this.publishUrl, JSON.stringify({id: id}))
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}

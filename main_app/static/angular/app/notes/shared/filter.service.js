"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
require("rxjs/add/operator/toPromise");
function getWeekNumber(d) {
    d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
    d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay() || 7));
    var yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
    var weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7);
    return weekNo;
}
var FilterService = (function () {
    function FilterService() {
    }
    FilterService.date = function (notes, type) {
        switch (type) {
            case 'today': {
                notes = notes.filter(function (note) { return (new Date(note.creation)).getDate() === (new Date()).getDate(); });
                break;
            }
            case 'week': {
                notes = notes.filter(function (note) { return getWeekNumber(new Date(note.creation)) === getWeekNumber(new Date()); });
                break;
            }
            case 'month': {
                notes = notes.filter(function (note) { return new Date(note.creation).getMonth() === (new Date()).getMonth(); });
                break;
            }
            case 'year': {
                notes = notes.filter(function (note) { return new Date(note.creation).getFullYear() === (new Date()).getFullYear(); });
                break;
            }
        }
        return notes;
    };
    FilterService.category = function (notes, category) {
        return notes.filter(function (note) { return note.category.id === category.id; });
    };
    FilterService.favourite = function (notes, val) {
        return notes.filter(function (note) { return note.favourite === val; });
    };
    FilterService.title = function (notes, substring) {
        return notes.filter(function (note) { return note.title.toLowerCase().indexOf(substring) !== -1; });
    };
    return FilterService;
}());
FilterService = __decorate([
    core_1.Injectable()
], FilterService);
exports.FilterService = FilterService;
//# sourceMappingURL=filter.service.js.map
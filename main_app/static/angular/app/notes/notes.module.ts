import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

// Imports for loading & configuring the in-memory web api


import {HttpModule} from '@angular/http';

import {NotesComponent} from './notes.component';
import {NoteService} from './shared/notes.service';
import {NotesRoutingModule} from "./notes-routing.module";
import {RouterModule} from "@angular/router";
import {BrowserModule} from "@angular/platform-browser";
import {CreateComponent} from "./create/create.component";
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import {UpdateComponent} from "./update/update.component";
import { MomentModule } from 'angular2-moment';
import {FilterService} from "./shared/filter.service";


@NgModule({
    imports: [
        FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),
        BrowserModule,
        MomentModule,
        FormsModule,
        HttpModule,
        RouterModule,
        NotesRoutingModule
    ],
    declarations: [
        NotesComponent,
        CreateComponent,
        UpdateComponent
    ],
    providers: [
        NoteService, FilterService
    ]
})
export class NotesModule {
}
